import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights(){
	return(
		<Row className="mt-3 mb-3">
                        <Col xs={12} md={4}>
                            <Card className="cardHighlight p-3">
                                <Card.Body>
                                    <Card.Title>
                                        <h2>Art to Cart</h2>
                                    </Card.Title>
                                    <Card.Text>
                                        There I was again tonight forcing laughter faking smiles same old tired lonely place walls of insincerity shifting eyes and vacancy vanished when I saw your face all I can say is it was enchanting to meet you.
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col xs={12} md={4}>
                            <Card className="cardHighlight p-3">
                                <Card.Body>
                                    <Card.Title>
                                        <h2>Wag mahiyang mag Adu to Cart</h2>
                                    </Card.Title>
                                    <Card.Text>
                                        Your eyes whispered Have we met accross the room your silhouette starts to make its way to me the playful conversation starts counter all your quick remarks like passing notes In secrecy
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col xs={12} md={4}>
                            <Card className="cardHighlight p-3">
                                <Card.Body>
                                    <Card.Title>
                                        <h2>Add color to your life</h2>
                                    </Card.Title>
                                    <Card.Text>
                                        And it was enchanting to meet you All I can say is I was enchanted to meet you This night is sparkling dont you let it go Im wonderstruck blushing all the way home Ill spend forever wondering if you knew I was enchanted to meet you.
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
		)
}