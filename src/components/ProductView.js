import {Row, Col, Button, Card} from 'react-bootstrap';
//the useParams allows us to get or extract the parameter included in our pages
import { useParams } from 'react-router-dom';
import {Link} from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import Swal2 from 'sweetalert2';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from "react-router-dom";

export default function ProductView(){
  	const { user, setUser } = useContext(UserContext);
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [quantity, setQuantity] = useState('');
	const [image, setImage] = useState('');
	const [age, setAge] = useState('');
	const [isActive, setIsActive] = useState(1);
	const [newQuan, setNewQuan] = useState(1);
	const {productId} = useParams();
	const navigate = useNavigate();

	
function addNewOrder(event) {
  const token = localStorage.getItem("token");
  fetch(`${process.env.REACT_APP_API_URL}/${user.id}/orders`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}` // Include the authorization header
    },
    body: JSON.stringify({
      productId: productId,
      quantity: newQuan,
      price: price
    })
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Failed to add new order");
      }
      return response.json();
    })
    .then((data) => {
      // Success: handle the response data
      console.log(data);
      Swal2.fire({
        title: "Order Placed",
        icon: "success",
        text: "Your order has been placed successfully!"
      });
    })
    .catch((error) => {
      // Error: handle the error
      console.error("Failed to add new order:", error.message);
      Swal2.fire({
        title: "Order Failed",
        icon: "error",
        text: "Something went wrong, please try again"
      });
    });
}



	/*console.log(id);*/

	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(response => response.json())
		.then(data => {
			// console.log(data);
				setName(data.name);
				setPrice(data.price);
				setDescription(data.description);
				setImage(data.image);
				setQuantity(data.quantity);
				setAge(data.age);
				setIsActive(data.isActive);
	
		})
	}, [])

/*	const order = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/order/`,{
			method: "POST",
			headers: {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: `${productId}`
			})
		})
		.then(response => response.json())
		.then(data => {
			// console.log(data);
			if(data === true){
				//successful yung login
				Swal2.fire({
					title: 'Order successful!',
					icon: 'success',
					text: 'Product is now at the cart!'
				})
			}else{
				Swal2.fire({
					title: 'Enrollment unsuccessful',
					icon: 'error',
					text: 'Please try again!'
				})
			}
		})
	}*/

	let discount;
    if (age < 13) {
         discount = Math.round(price * 1.2  * 100) / 100;
    } else if ((age >= 13 && age <= 21) || age >= 65) {
        //const discountedPrice = price * 0.8.toFixed(2); 
         discount = Math.round(price * 1.3  * 100) / 100;
    } else {
        //const roundedPrice = price.toFixed(2);
         discount = Math.round(price * 1.4  * 100) / 100;     
    }

	return(
		<Card className="container mt-3">
		  <Card.Body className="row">
		    <div className="col-3">
		    <img className="card-img-top" src={image}/>
		    </div>
		    <div className="col-7">
		      <Card.Title>{name}</Card.Title>
		      <Card.Subtitle>Description:</Card.Subtitle>
		      <Card.Text>{description}</Card.Text>
		    </div>
		    <div className="col-2 text-left">
		      <Card.Subtitle>Price:</Card.Subtitle>
		      <Card.Text>{price.toLocaleString("en-PH", { style: "currency", currency: "PHP" })}</Card.Text>
          	<Card.Subtitle>Before:</Card.Subtitle>
		      <Card.Text className="text-warning" style={{ textDecoration: 'line-through' }}>
            {discount.toLocaleString("en-PH", { style: "currency", currency: "PHP" })}
          	</Card.Text>
		                <Card.Text>
				            {isActive ? `Stocks: ${quantity}` : "Out of Stock"}
				          </Card.Text>
					<Form>
					  <Form.Group controlId="quantity">
					    <div className="input-group">
					      <Button variant="outline-secondary" id="btn-minus" onClick={() => setNewQuan(newQuan - 1)} disabled={newQuan === 1}>-</Button>
					      <Form.Control min={1} value={newQuan} onChange={(e) => setNewQuan(parseInt(e.target.value))} className="text-center" />
					      <Button variant="outline-secondary" id="btn-plus" onClick={() => setNewQuan(newQuan + 1)} disabled={quantity === newQuan}>+</Button>
					    </div>
					  </Form.Group>
					</Form>					   
			           <Button variant="primary" className="mt-3 text-center col-md-12" onClick={addNewOrder}>
		                 Add to Cart
		               </Button>
		    </div>
		  </Card.Body>
		</Card>
		)
}


