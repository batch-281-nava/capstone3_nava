import { Button, Modal, Form } from 'react-bootstrap';
import { useState } from 'react';
import { useNavigate } from "react-router-dom";
import Swal2 from "sweetalert2";

export default function AdminModal() {
  const [show, setShow] = useState(false);
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [category, setCategory] = useState('');
  const [price, setPrice] = useState('');
  const [quantity, setQuantity] = useState('');
  const [image, setImage] = useState('');
  const navigate = useNavigate();

  function addNewProduct(e) {
    e.preventDefault();

    const token = localStorage.getItem("token"); // Assuming you store the token in localStorage after login

    fetch(`${process.env.REACT_APP_API_URL}/products/`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}` // Include the authorization header
      },
      body: JSON.stringify({
        name: name,
        description: description,
        category: category,
        price: price,
        quantity: quantity,
        image: image
      })
    })
      .then((response) => response.json())
      .then((data) => {
        if (data) {
          Swal2.fire({
            title: "Adding Product Successfuly",
            icon: "yehey!",
            text: "Congrats!"
          });
          navigate("/products");
        } else {
          Swal2.fire({
            title: "Adding Product Failed",
            icon: "error",
            text: "Something went wrong, try again"
          });
        }
      });
      setShow(false);
  }






  return (
    <>
      <Button variant="primary" className="mt-3 mb-3" onClick={() => setShow(true)}>
        Add New Product
      </Button>

      <Modal show={show} onHide={() => setShow(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Enter Product Details</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={addNewProduct}>
            <Form.Group className="mb-3" controlId="name">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                type="text"
                value={name}
                onChange={(e) => setName(e.target.value)}
                placeholder="Enter Product Name"
                autoFocus
              />
            </Form.Group>
	            <Form.Group className="mb-3" controlId="description">
	              <Form.Label>Product Description</Form.Label>
	              <Form.Control
	                type="text"
	                placeholder="Product Description"
	                value={description}
	                onChange={(e) => setDescription(e.target.value)}
	                autoFocus
	              />
	            </Form.Group>
	             <Form.Group className="mb-3" controlId="price">
	              <Form.Label>Product Price</Form.Label>
	              <Form.Control
	                type="number"
	                value={price}
	                onChange={(e) => setPrice(e.target.value)}
	                placeholder="PHP"
	                autoFocus
	              />
	            </Form.Group>
	             <Form.Group className="mb-3" controlId="category">
	              <Form.Label>Product Category</Form.Label>
	              <Form.Control
	                type="text"
	                value={category}
	                onChange={(e) => setCategory(e.target.value)}
	                autoFocus
	              />
	            </Form.Group>
	             <Form.Group className="mb-3" controlId="quantity">
	              <Form.Label>Product Quantity</Form.Label>
	              <Form.Control
	                type="number"
	                value={quantity}
	                onChange={(e) => setQuantity(e.target.value)}
	                placeholder="1"
	                autoFocus
	              />
	            </Form.Group>
	            <Form.Group className="mb-3" controlId="image">
	              <Form.Label>Product Image</Form.Label>
	              <Form.Control
	                type="text"
	                value={`https://drive.google.com/uc?export=view&id=${image}`}
	                onChange={(e) => setImage(e.target.value)}
	                placeholder="link..."
	                autoFocus
	              />
	            </Form.Group>	 	           
	                 </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShow(false)}>
            Close
          </Button>
          <Button variant="primary" onClick={addNewProduct}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}