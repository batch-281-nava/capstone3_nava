import {Row, Col, Button, Card} from 'react-bootstrap';
//the useParams allows us to get or extract the parameter included in our pages
import { useParams } from 'react-router-dom';
import {Link} from 'react-router-dom';
import { useState, useEffect } from 'react';

import Swal2 from 'sweetalert2';

export default function ProductView(){

const [name, setName] = useState('');
const [description, setDescription] = useState('');
const [price, setPrice] = useState('');
const [quantity, setQuantity] = useState('');
const [image, setImage] = useState('');
const [age, setAge] = useState('');
const [isActive, setIsActive] = useState('');

const {productId} = useParams();



/*console.log(id);*/

	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(response => response.json())
		.then(data => {
			// console.log(data);
				setName(data.name);
				setPrice(data.price);
				setDescription(data.description);
				setImage(data.image);
				setQuantity(data.quantity);
				setAge(data.age)
				setIsActive(data.isActive);
		})
	}, [])

/*	const order = (productId) => {
	fetch(`${process.env.REACT_APP_API_URL}/order/`,{
		method: "POST",
		headers: {
			'Content-Type' : 'application/json',
			'Authorization' : `Bearer ${localStorage.getItem('token')}`
		},
		body: JSON.stringify({
			productId: `${productId}`
		})
	})
	.then(response => response.json())
	.then(data => {
		// console.log(data);
		if(data === true){
			//successful yung login
			Swal2.fire({
				title: 'Order successful!',
				icon: 'success',
				text: 'Product is now at the cart!'
			})
		}else{
			Swal2.fire({
				title: 'Enrollment unsuccessful',
				icon: 'error',
				text: 'Please try again!'
			})
		}
	})
}*/

let discount;
if (age < 13) {
     discount = Math.round(price * 1.2  * 100) / 100;
} else if ((age >= 13 && age <= 21) || age >= 65) {
    //const discountedPrice = price * 0.8.toFixed(2); 
     discount = Math.round(price * 1.3  * 100) / 100;
} else {
    //const roundedPrice = price.toFixed(2);
     discount = Math.round(price * 1.4  * 100) / 100;     
}

return(
	<Card className="container">
	  <Card.Body className="row">
	    <div className="col-3">
	    <img className="card-img-top" src={image}/>
	    </div>
	    <div className="col-7">
	      <Card.Title>{name}</Card.Title>
	      <Card.Subtitle>Description:</Card.Subtitle>
	      <Card.Text>Product is Unavailable at the moment</Card.Text>
	    </div>
	    <div className="col-2">
	      <Card.Subtitle>Price:</Card.Subtitle>
	      <Card.Text>{price}</Card.Text>
	      <Card.Subtitle>Before:</Card.Subtitle>
	      <Card.Text style={{textDecoration: 'line-through'}}>{discount}</Card.Text>
	      <Card.Text>Stocks: {quantity}</Card.Text>
	    </div>
	  </Card.Body>
	</Card> 


	)
}


