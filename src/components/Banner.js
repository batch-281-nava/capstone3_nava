import {Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';
export default function Banner(){
	return(
		<Row>
		    <Col className="p-5">
		        <h1>Thank You for using Sketchy!</h1>
		        <p>Put some Art in your Cart.</p>
		        <Button as = {Link} to = '/products' variant = "primary">Adu to cart now!</Button>
		    </Col>
		</Row>
	);
}