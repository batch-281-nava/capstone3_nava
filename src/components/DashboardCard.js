import React from 'react';
import { Card, Button , Modal, Form, Table } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Fragment } from 'react';
import Swal2 from 'sweetalert2';
import { useParams } from 'react-router-dom';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from "react-router-dom";


function DashboardCard({ productProp }) {
  // State hooks for the products, loading, error and modal
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [showModal, setShowModal] = useState(false);

  // State hooks for the product form inputs
  const [name, setname] = useState("");
  const [description, setdescription] = useState("");
  const [price, setprice] = useState(0);
  const [image, setImage] = useState("");

  // State hook for the selected product id
  const [selectedProductId, setSelectedProductId] = useState(null);

  // Context hook for the user data
  const { user } = useContext(UserContext);

  // Effect hook for fetching all products
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then((response) => response.json())
      .then((data) => {
        console.log(data)
        setProducts(data);
        setLoading(false);
      })
      .catch((error) => {
        setError(error.message);
        setLoading(false);
      });
  }, []);

  // Function for handling the modal show and hide
  const handleShowModal = () => setShowModal(true);
  const handleCloseModal = () => setShowModal(false);

  // Function for handling the product creation
  const handleCreateProduct = () => {
    // Create an object with the product form inputs
    const newProductData = {
      name: name,
      description: description,
      price: price,
      image: image,
    };
    // Send a post request to the backend create product route with the new product data and the user token
    fetch(`${process.env.REACT_APP_API_URL}/products/createProduct`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify(newProductData)
    })
    .then((response) => response.json())
    .then((data) => {
        // If the product creation is successful, show a success message and update the products state
        if (data._id) {
          Swal2.fire({
            title: "Sketch Added!",
            icon: "yehey!",
            text: "Sketch Added successfully!"
          });
          setProducts([...products, data]);
          handleCloseModal();
        } else {
          // If the product creation is not successful, show an error message
          Swal2.fire({
            title: "Error Adding Sketch!",
            icon: "error",
            text: "Please try again"
          });
        }
      })
      .catch((error) => console.log(error));
  };

  // Function for handling the product update
  const handleUpdateProduct = () => {
    // Create an object with the product form inputs
    const updatedProductData = {
      name: name,
      description: description,
      price: price,
      image: image,
    };
    // Send a put request to the backend update product route with the updated product data and the user token
    fetch(
      `${process.env.REACT_APP_API_URL}/products/update/${selectedProductId}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify(updatedProductData),
      }
    )
      .then((response) => response.json())
      .then((data) => {
        // If the product update is successful, show a success message and update the products state
        if (data._id) {
          Swal2.fire({
            title: "Sketch order updated!",
            icon: "yehey!",
            text: data,
          });
          setProducts(
            products.map((product) =>
              product._id === selectedProductId ? data : product
            )
          );
          handleCloseModal();
        } else {
          // If the product update is not successful, show an error message
          Swal2.fire({
            title: "Product update failed!",
            icon: "error",
            text: data,
          });
        }
      })
      .catch((error) => console.log(error));
  };

  // Function for handling the product activation or deactivation
  const handleToggleActive = (productId, isActive) => {
    // Create an object with the opposite value of isActive
    const toggleData = {
      isActive: !isActive,
    };
    // Send a patch request to the backend activate or archive product route with the toggle data and the user token
    fetch(
      `${process.env.REACT_APP_API_URL}/products/${productId}/${
        isActive ? "archives" : "activate"
      }`,
      {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify(toggleData),
      }
    )
      .then((response) => response.json())
      .then((data) => {
        // If the product activation or deactivation is successful, show a success message and update the products state
        if (data) {
          Swal2.fire({
            title: `Product ${isActive ? "deactivated" : "activated"}!`,
            icon: "success",
            text: `You have successfully ${
              isActive ? "deactivated" : "activated"
            } the product.`,
          });
          setProducts(
            products.map((product) =>
              product._id === productId
                ? { ...product, isActive: !isActive }
                : product
            )
          );
        } else {
          // If the product activation or deactivation is not successful, show an error message
          Swal2.fire({
            title: `Product ${isActive ? "deactivation" : "activation"} failed!`,
            icon: "error",
            text: data,
          });
        }
      })
      .catch((error) => console.log(error));
  };

  // Function for handling the modal form change
  const handleFormChange = (e) => {
    switch (e.target.name) {
      case "name":
        setname(e.target.value);
        break;
      case "description":
        setdescription(e.target.value);
        break;
      case "price":
        setprice(e.target.value);
        break;
      default:
        break;
    }
  };

  // Function for handling the modal form submit
  const handleFormSubmit = (e) => {
    e.preventDefault();
    if (selectedProductId) {
      handleUpdateProduct();
    } else {
      handleCreateProduct();
    }
  };

  // Function for handling the modal form reset
  const handleFormReset = () => {
    setname("");
    setdescription("");
    setprice(0);
    setSelectedProductId(null);
  };

  // Function for handling the edit button click
  const handleEditClick = (productId) => {
    const productToEdit = products.find((product) => product._id === productId);
    setname(productToEdit.name);
    setdescription(productToEdit.description);
    setprice(productToEdit.price);
    setImage(productToEdit.image);
    setSelectedProductId(productId);
    handleShowModal();
  };


  const renderTableRows = () => {
    return products.map((product) => (
      <tr key={product._id}>
        <td>{product.name}</td>
        <td>{product.description}</td>
        <td>{`₱ ${product.price}`}</td>
        <td>{product.isActive ? "Active" : "Inactive"}</td>
        <td>
          <Button
            variant="dark"
            onClick={() =>
              handleToggleActive(product._id, product.isActive)
            }
          >
            {product.isActive ? "Deactivate" : "Activate"}
          </Button>
          <Button variant="dark" onClick={() => handleEditClick(product._id)}>
            Edit
          </Button>
        </td>
      </tr>
    ));
  };

  if (user.isAdmin) {
    return (
      <Fragment>
        {loading && <p>Loading...</p>}
        {error && <p>{error}</p>}
        {products && (
          <Fragment>
            

            <Table striped bordered hover variant="dark">
              <thead>
                <tr>
                  <th>Product Name</th>
                  <th>Product Description</th>
                  <th>Product Price</th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>{renderTableRows()}</tbody>
            </Table>

            <Modal show={showModal} onHide={handleCloseModal}>
              <Modal.Header closeButton>
                <Modal.Title>
                  {selectedProductId ? "Update Product" : "Create Product"}
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Form onSubmit={handleFormSubmit} onReset={handleFormReset}>
                  <Form.Group controlId="name">
                    <Form.Label>Product Name</Form.Label>
                    <Form.Control
                      type="text"
                      name="name"
                      value={name}
                      onChange={handleFormChange}
                      required
                    />
                  </Form.Group>
                  <Form.Group controlId="description">
                    <Form.Label>Product Description</Form.Label>
                    <Form.Control
                      as="textarea"
                      name="description"
                      value={description}
                      onChange={handleFormChange}
                      required
                    />
                  </Form.Group>
                  <Form.Group controlId="price">
                    <Form.Label>Product Price</Form.Label>
                    <Form.Control
                      type="number"
                      name="price"
                      value={price}
                      onChange={handleFormChange}
                      required
                    />
                  </Form.Group>
                  <Form.Group controlId="productImage">
                    <Form.Label>Product Image URL</Form.Label>
                    <Form.Control
                      type="text"
                      name="productImage"
                      value={image}
                      onChange={(e) => setImage(e.target.value)}
                            required
                    />
                  </Form.Group>
                  <Modal.Footer>
                    <Button variant="secondary" type="reset">
                      Clear
                    </Button>
                    <Button variant="primary" type="submit">
                      {selectedProductId ? "Update" : "Create"}
                    </Button>
                  </Modal.Footer>
                </Form>
              </Modal.Body>
            </Modal>
          </Fragment>
        )}
      </Fragment>
    );
  } else {
    return <p>You are not authorized to access this page.</p>;
  }
}

export default DashboardCard;