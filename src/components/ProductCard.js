import { Card, Button , Modal, Form} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useState, useEffect, useContext } from 'react';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from "react-router-dom";
import Swal2 from "sweetalert2";

export default function ProductCard({productProp}) {
  const [products, setProducts] = useState([]);
  const {_id, name, description, price, quantity, category, image, isActive} = productProp;
  const [show, setShow] = useState(false);
  const navigate = useNavigate();
  const [updateShow, setUpdateShow] = useState(false);
  const [deleteShow, setDeleteShow] = useState(false);
  const { user, setUser } = useContext(UserContext);
  const [name1, setName1] = useState(name);
  const [description1, setDescription1] = useState(description);
  const [category1, setCategory1] = useState(category);
  const [price1, setPrice1] = useState(price);
  const [email1, setEmail1] = useState("");
  const [quantity1, setQuantity1] = useState(quantity);
  const [image1, setImage1] = useState(image);
  const [isActive1, setIsActive1] = useState(isActive);

// Define modal open functions
const handleUpdateShow = () => {
  setUpdateShow(true);
};

const handleDeleteShow = () => {
  setDeleteShow(true);
};

// Define modal close functions
const handleUpdateClose = () => {
  setUpdateShow(false);
};

const handleDeleteClose = () => {
  setDeleteShow(false);
};

// Define delete action function
const handleDelete = () => {
  // Handle delete logic
};
 

    // console.log(name);

    // Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components
    // const [discount, setDiscount] = useState();
    // const [seats, setSeats] = useState(30);

    //we are goinbg to create a new state that will declare or tell the value of the disabled property in the button.
    const [isDisabled, setIsDisabled] = useState(false);
let discount;
    if (user.age < 25) {
         discount = Math.round(price * 1.1  * 100) / 100;
    } else{
        //const discountedPrice = price * 0.8.toFixed(2); 
         discount = Math.round(price * 1.4  * 100) / 100;
    }

function updateProduct(event) {
  const token = localStorage.getItem("token");
    fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`, {
    method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}` // Include the authorization header
      },
    body: JSON.stringify({
      name: name1,
      description: description1,
      price: price1,
      category: category1,
      quantity: quantity1,      
      image: image1
    })
  })
    .then((response) => response.json())
    .then((data) => {
      // console.log(data);
      if (data) {
        Swal2.fire({
          title: "Sketch Updated",
          icon: "yehey!",
          text: "Sketch updated Successfuly!"
        });
        
      } else {
        Swal2.fire({
          title: "Sketch update Failed",
          icon: "error",
          text: "Something went wrong, please try again"
        });
      }
      navigate(`/products/${_id}`);
    })
  
setShow(false);

}

function deleteProduct(event) {
  const token = localStorage.getItem("token");
    fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
    method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}` // Include the authorization header
      },
      body: JSON.stringify({
      name: name1,
      description: description1,
      price: price1,
      category: category1,
      quantity: quantity1,      
      image: image1
    })
  })
    .then((response) => response.json())
    .then((data) => {
      // console.log(data);
      if (data) {
        Swal2.fire({
          title: "Delete Successful",
          icon: "success!",
          text: "Sketch order deleted Successfuly!"
        });
        
      } else {
        Swal2.fire({
          title: "Sketch Update Failed",
          icon: "error",
          text: "Something went wrong, please try again"
        });
      }
      navigate(`/products/${_id}`);
    })
  
setShow(false);

}





function activateProduct(event) {
  const token = localStorage.getItem("token");
    fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/activate`, {
    method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}` // Include the authorization header
      },
      body: JSON.stringify({
      name: name1,
      description: description1,
      price: price1,
      category: category1,
      quantity: quantity1,      
      image: image1
    })
  })
    .then((response) => response.json())
    .then((data) => {
      // console.log(data);
      if (data) {
        Swal2.fire({
          title: "Sketch Activated ",
          icon: "yehey!",
          text: "Sketch Activated Successfuly!"
        });
        
      } else {
        Swal2.fire({
          title: "Activation Failed",
          icon: "error",
          text: "Something went wrong, please try again"
        });
      }
      navigate(`/products/${_id}`);
    })
  
setShow(false);

}

  return (
  <>
    <Card className="container mb-3 mt-3">
      <Card.Body className="row">
        <div className="col-3">
          <img className="card-img-top" src={image} alt="drive image" />
        </div>
        <div className="col-7">
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
        </div>
        <div className="col-2 border-left-1 border-dark">
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>{price.toLocaleString("en-PH", { style: "currency", currency: "PHP" })}</Card.Text>
          <Card.Subtitle>Before:</Card.Subtitle>
          <Card.Text className="text-warning" style={{ textDecoration: 'line-through' }}>
            {discount.toLocaleString("en-PH", { style: "currency", currency: "PHP" })}
          </Card.Text>
          <Card.Text>
            {isActive1 ? `Stocks: ${quantity}` : "Not available at the moment"}
          </Card.Text>
          <Button as={Link} to={`/products/${_id}`} variant="primary" disabled={isDisabled}>
            See more details
          </Button>
          {user.isAdmin && (
            <>
              <Button variant="success" className="mt-3 mx-1" onClick={handleUpdateShow}>
                Update
              </Button>
              {isActive ? (
                <Button variant="danger" className="mt-3 mx-1" onClick={deleteProduct}>
                  Delete
                </Button>
              ) : (
                <Button variant="danger" className="mt-3 mx-1" onClick={activateProduct}>
                  Activate
                </Button>
              )}
            </>
          )}
          {/* Update Modal */}
          <Modal show={updateShow} onHide={handleUpdateClose}>
           <Modal.Header closeButton>
            <Modal.Title>Enter Product Details</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                <Form.Label>Product Name</Form.Label>
                <Form.Control
                  type="text"
                  value={name1}
                  defaultValue={name1}                  
                  onChange={(e) => {
                      if (e.target.value === name) {
                        setName1(name); // Set name1 back to the default value
                      } else {
                        setName1(e.target.value);
                      }
                    }}
                  placeholder="Enter Product Name"
                  autoFocus
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
                <Form.Label>Product Description</Form.Label>
                <Form.Control
                  type="text"
                  value={description1}
                  onChange={(e) => {
                      if (e.target.value === description) {
                        setDescription1(description); // Set name1 back to the default value
                      } else {
                        setDescription1(e.target.value);
                      }
                    }}
                  placeholder="Product Description"
                  autoFocus
                />
                </Form.Group>
              <Form.Group className="mb-3" controlId="exampleForm.ControlInput3">
                <Form.Label>Product Price</Form.Label>
                <Form.Control
                  type="number"
                  value={price1}
                  onChange={(e) => {
                    if (e.target.value === price) {
                      setPrice1(price); // Set price1 back to the default value
                    } else {
                      setPrice1(e.target.value);
                    }
                  }}
                  placeholder="PHP"
                  autoFocus
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="exampleForm.ControlInput6">
                <Form.Label>Category </Form.Label>
                <Form.Control
                  type="text"
                  value={category1}
                  onChange={(e) => {
                    if (e.target.value === category) {
                      setCategory1(category); // Set name1 back to the default value
                    } else {
                      setCategory1(e.target.value);
                    }
                  }}
                  placeholder="link..."
                  autoFocus
                />
              </Form.Group>   
               <Form.Group className="mb-3" controlId="exampleForm.ControlInput4">
                <Form.Label>Product Quantity</Form.Label>
                <Form.Control
                  type="number"
                  value={quantity1}
                  onChange={(e) => {
                    if (e.target.value === quantity) {
                      setQuantity1(quantity); // Set name1 back to the default value
                    } else {
                      setQuantity1(e.target.value);
                    }
                  }}
                  placeholder="1"
                  autoFocus
                />
              </Form.Group>
               <Form.Group className="mb-3" controlId="exampleForm.ControlInput5">
                <Form.Label>Image Link </Form.Label>
                <Form.Control
                  type="text"
                  value={image1 || image}
                  onChange={(e) => setImage1(e.target.value)}
                  placeholder="link..."
                  autoFocus
                />
              </Form.Group> 
                    
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleUpdateClose}>
              Close
            </Button>
            <Button variant="primary" onClick={updateProduct}>
              Save Changes
            </Button>
          </Modal.Footer>
          </Modal>

          {/* Delete Modal */}
          <Modal show={deleteShow} onHide={handleDeleteClose}>
             <Modal.Header closeButton>
              <Modal.Title>Delete Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <p>Your product will be deleted, Proceed?</p>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleDeleteClose}>
                Cancel
              </Button>
              <Button variant="danger" onClick={handleDelete}>
                Delete
              </Button>
            </Modal.Footer>
          </Modal>
        </div>
      </Card.Body>
    </Card>
  </>
)
}

// Check if the CourseCard component is getting the correct prop types
// Proptypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is being passed from one component to the next
// CourseCard.propTypes = {
//     // The "shape" method is sued to check if a prop object conforms to a specific shape
//     courseProp: PropTypes.shape({
//         name: PropTypes.string.isRequired,
//         description: PropTypes.string.isRequired,
//         price: PropTypes.number.isRequired
//     })
// }

