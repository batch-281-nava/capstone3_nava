import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import PropTypes from 'prop-types';
export default function Home(){
	
	return (
		<Fragment>
			<Banner />
			<Highlights />
		</Fragment>
	)
}
